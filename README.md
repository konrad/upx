# UPX in docker

[![Build Status](https://drone1.kolaente.de/api/badges/konrad/upx/status.svg)](https://drone1.kolaente.de/konrad/upx)

This dockerfile contains [upx](https://upx.github.io/) in a docker container to be able to use it more easily in a ci pipeline

## How to use this image

```
docker run -v <local path to binaries>:/binaries kolaente/upx /binaries/<binary file name>
```
