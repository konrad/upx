FROM alpine:latest

RUN apk add --update wget make tar xz && \
  wget https://github.com/upx/upx/releases/download/v3.95/upx-3.95-amd64_linux.tar.xz && \
  tar xf upx-3.95-amd64_linux.tar.xz && \
  mv upx-3.95-amd64_linux/upx /usr/local/bin

RUN wget https://github.com/magefile/mage/releases/download/v1.10.0/mage_1.10.0_Linux-64bit.tar.gz && \
  tar xf mage_1.10.0_Linux-64bit.tar.gz && \
  mv mage /usr/local/bin

ENTRYPOINT ["upx"]

